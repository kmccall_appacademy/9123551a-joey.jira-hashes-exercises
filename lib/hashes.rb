# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_hash = Hash.new(0)

  str.split.each do |word|
    word_hash[word] = word.length
  end

  word_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.max_by { |_key, value| value }[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older[key] = value
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  word_count = Hash.new(0)

  word.chars.each do |ch|
    word_count[ch] += 1
  end

  word_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hash = Hash.new(0)
  arr.each do |element|
    uniq_hash[element] = 0
  end

  uniq_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  numb_type = {:even => 0, :odd => 0}

  numbers.each do |number|
    numb_type[:even] += 1 if number.even?
    numb_type[:odd] += 1 if number.odd?
  end

  numb_type
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  counts = Hash.new(0)

  string.chars.each do |ch|
    counts[ch] += 1 if vowels.include?(ch.downcase)
  end

  greatest_key_by_val(counts.sort)
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half = students.select { |_, value| value > 6 }.keys
  result = []

  second_half.each_index do |idx1|
    second_half.each_index do |idx2|
      next if idx1 == idx2
      unless result.include?([second_half[idx1], second_half[idx2]].sort)
        result << [second_half[idx1], second_half[idx2]]
      end
    end
  end

  result
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimens_count = Hash.new(0)

  specimens.each do |animal|
    specimens_count[animal] += 1
  end

  number_of_species = specimens_count.length
  smallest_population_size = specimens_count.min_by { |_, v| v }.last
  largest_population_size = specimens_count.max_by { |_, v| v}.last

  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandalized_count = character_count(vandalized_sign)

  normal_count.each do |key, _value|
    return false if normal_count[key] < vandalized_count[key]
  end

  return true
end

def character_count(str)
  str.delete!(" .,;:!?")

  ch_count = Hash.new(0)

  str.chars.each do |ch|
    ch_count[ch.downcase] += 1
  end

  ch_count
end
